clc
clear
pi=3.1415;
f= 0.61;%0.031830988655; %primary frequency of the tower
wc= 2*pi*f*0.3

ThetaD=0.873; % design phase margin rad, 50deg
gamma=-3.04*10^5 ;%proportionality constant in the change of the pitch angle  (Nm/rad)
J= 6.45*10^6;%4.3783e+07 ; %Sum of the moment of inertia of the moment of inertia 
        %of the rotor and the generator of standard low-speed shaft
        %( generator moment of inertia x r2) kgm^2
Ta= 0.3 ; % Pitch actuator time constant s  (Assumed)
r= 83.3 %speed increasing ratio
delta= -4.13*10^6; %proportionality constant in the change of the pitch
                        %pitch angle Nm/rad        
 %%       
CoeffTheta= (gamma+J*Ta*wc*wc)/((gamma*Ta-J)*wc)
ThetaM=(atan(CoeffTheta))-pi

%%


Tsi= (tan(ThetaD-ThetaM)/wc)


CoeffKsp= -(Tsi*wc)/(r*delta);
CoeffKsp2= (((gamma*gamma)+(J*J)*(wc*wc))*(1+(Ta*Ta)*(wc*wc)))/(1+(Tsi*Tsi)*(wc*wc))
Ksp= CoeffKsp* sqrt(CoeffKsp2)

Ksi=Ksp/Tsi
