clear all;
clc;
%% Importing DATA
FAST=importdata(['C:\FAST\Water Tank 2D Cd096 2015\WaterTank2D FreeDecay\HeaveOut.dat']);
%FAST=FAST.data;
FAST2=FAST(:,4);


Exp= importdata(['C:\FAST\Water Tank 2D Cd096 2015\WaterTank2D FreeDecay\Results Matlab\Heave_Exp.txt']);
CAST=importdata(['C:\FAST\Water Tank 2D Cd096 2015\WaterTank2D FreeDecay\Results Matlab\Heave_CAsT.out']);
Bladed=importdata(['C:\FAST\Water Tank 2D Cd096 2015\WaterTank2D FreeDecay\Results Matlab\Heave_Bladed.txt']);
Bladed=Bladed.data;
Bladed2= Bladed(:,2)+0.167;
%%
Experiment(:,1)=(Exp(393:end,1))-(Exp(393,1));
Experiment(:,2)=(Exp(393:end,4));
%% Plot Pitch Responce
fig=figure(1);
    set(gca,'FontSize',15);
       plot(Experiment(:,1),Experiment(:,2),'green','linewidth',2);
       hold on 
        plot(Bladed(:,1),Bladed2(:,1),'b','linewidth',2);
        plot(CAST(:,1),CAST(:,4),'r','linewidth',2);
        plot(FAST(:,1),FAST2(:,1),'k','linewidth',2);
    hold off
      xlabel('Time (sec))','fontsize',20);
     xlim([0 20]);
    ylabel('Heave [m]','fontsize',20);
    %set(gca,'yscale','log')
    %ylim([1E-10 1E2]);
    % title('Pitch ','position',[0.04 0.8E1],'fontsize',20);
 legend('Exp','Bladed','CAST','FAST')
    saveas(fig,'HeaveFreeDecay.jpg');
