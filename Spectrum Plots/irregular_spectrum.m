clear all;
clc;
Output=importdata(['C:\FAST\Irregular Wave\H0.102T1.31_B_r5.6.txt']);
xx_Bladed=Output.data;
xx_CAsT=importdata(['C:\FAST\Irregular Wave\H0.1T1.41.out']);
xx_Exp=importdata(['C:\FAST\Irregular Wave\Exp_H0.1_motion.txt']);
%% Exp raw data %%%%%%%%%%%
xs_Exp=xx_Exp(1501:10000,2);
[Sus_Exp,fs_Exp]=pwelch(xs_Exp-mean(xs_Exp),hamming(1024),0,8500,100);
Sus_Exp(1)=[];
fs_Exp(1)=[];
Su_Exp(:,1)=fs_Exp;
Su_Exp(:,2)=Sus_Exp;
% eval(sprintf('save spec_dynamicparked_surge_Exp.out -ascii Su_Exp'));

ys_Exp=xx_Exp(1501:10000,4);
[Sus_Exp,fs_Exp]=pwelch(ys_Exp-mean(ys_Exp),hamming(1024),0,8500,100);
Sus_Exp(1)=[];
fs_Exp(1)=[];
He_Exp(:,1)=fs_Exp;
He_Exp(:,2)=Sus_Exp;
% eval(sprintf('save spec_dynamicparked_heave_Exp.out -ascii He_Exp'));

zs_Exp=xx_Exp(1501:10000,6);
[Sus_Exp,fs_Exp]=pwelch(zs_Exp-mean(zs_Exp),hamming(1024),0,8500,100);
Sus_Exp(1)=[];
fs_Exp(1)=[];
Pi_Exp(:,1)=fs_Exp;
Pi_Exp(:,2)=Sus_Exp;
% eval(sprintf('save spec_dynamicparked_pitch_Exp.out -ascii Pi_Exp'));


%% Bladed result %%%%%%%%%%%
xs_Bladed=xx_Bladed(1501:10000,2);
[Sus_Bladed,fs_Bladed]=pwelch(xs_Bladed-mean(xs_Bladed),hamming(1024),0,8500,100);
Sus_Bladed(1)=[];
fs_Bladed(1)=[];
Su_Bladed(:,1)=fs_Bladed;
Su_Bladed(:,2)=Sus_Bladed;
% eval(sprintf('save spec_dynamicparked_surge_Bladed.out -ascii Su_Bladed'));

ys_Bladed=xx_Bladed(1501:10000,4);
[Sus_Bladed,fs_Bladed]=pwelch(ys_Bladed-mean(ys_Bladed),hamming(1024),0,8500,100);
Sus_Bladed(1)=[];
fs_Bladed(1)=[];
He_Bladed(:,1)=fs_Bladed;
He_Bladed(:,2)=Sus_Bladed;


zs_Bladed=xx_Bladed(1501:10000,6);
[Sus_Bladed,fs_Bladed]=pwelch(zs_Bladed-mean(zs_Bladed),hamming(1024),0,8500,100);
Sus_Bladed(1)=[];
fs_Bladed(1)=[];
Pi_Bladed(:,1)=fs_Bladed;
Pi_Bladed(:,2)=Sus_Bladed;


%% CAsT result %%%%%%%%%%%
xs_CAsT=xx_CAsT(1501:10000,2);
[Sus_CAsT,fs_CAsT]=pwelch(xs_CAsT-mean(xs_CAsT),hamming(1024),0,8500,100);
Sus_CAsT(1)=[];
fs_CAsT(1)=[];
Su_CAsT(:,1)=fs_CAsT;
Su_CAsT(:,2)=Sus_CAsT;


ys_CAsT=xx_CAsT(1501:10000,4);
[Sus_CAsT,fs_CAsT]=pwelch(ys_CAsT-mean(ys_CAsT),hamming(1024),0,8500,100);
Sus_CAsT(1)=[];
fs_CAsT(1)=[];
He_CAsT(:,1)=fs_CAsT;
He_CAsT(:,2)=Sus_CAsT;
% eval(sprintf('save spec_dynamicparked_heave.out -ascii He'));

zs_CAsT=xx_CAsT(1501:10000,6);
[Sus_CAsT,fs_CAsT]=pwelch(zs_CAsT-mean(zs_CAsT),hamming(1024),0,8500,100);
Sus_CAsT(1)=[];
fs_CAsT(1)=[];
Pi_CAsT(:,1)=fs_CAsT;
Pi_CAsT(:,2)=Sus_CAsT;
% eval(sprintf('save spec_dynamicparked_pitch.out -ascii Pi'));

%% Plot the spectrum of the motion.
fig=figure(1);
    set(gca,'FontSize',15);
       plot(Su_Exp(:,1),Su_Exp(:,2),'green','linewidth',2);
       hold on 
        plot(Su_Bladed(:,1),Su_Bladed(:,2),'b','linewidth',2);
    plot(Su_CAsT(:,1),Su_CAsT(:,2),'r','linewidth',2);
    hold off
      xlabel('Frequency (Hz)','fontsize',15);
      xlim([0 2]);
    ylabel('Power Spectrum [m^2s]','fontsize',15);
    set(gca,'yscale','log')
    ylim([1E-9 1E-2]);
      %  Title('Surge','position',[0.2 1E-3]);
 legend('Exp.','Bladed','CAsT')
    saveas(fig,'PSD-Surge.bmp');
    
    fig=figure(2);
    set(gca,'FontSize',15);
       plot(He_Exp(:,1),He_Exp(:,2),'green','linewidth',2);
       hold on 
        plot(He_Bladed(:,1),He_Bladed(:,2),'b','linewidth',2);
    plot(He_CAsT(:,1),He_CAsT(:,2),'r','linewidth',2);
    hold off
      xlabel('Frequency (Hz)','fontsize',15);
      xlim([0 2]);
    ylabel('Power Spectrum [m^2s]','fontsize',15);
    set(gca,'yscale','log')
    ylim([1E-9 1E-2]);
%     Title('Heave','position',[0.2 1E-3]);
 legend('Exp.','Bladed','CAsT')
    saveas(fig,'PSD-Heave.bmp');
    
    fig=figure(3);
    set(gca,'FontSize',15);
       plot(Pi_Exp(:,1),Pi_Exp(:,2),'green','linewidth',2);
       hold on 
        plot(Pi_Bladed(:,1),Pi_Bladed(:,2),'b','linewidth',2);
    plot(Pi_CAsT(:,1),Pi_CAsT(:,2),'r','linewidth',2);
    hold off
      xlabel('Frequency (Hz)','fontsize',15);
      xlim([0 2]);
    ylabel('Power Spectrum [m^2s]','fontsize',15);
    set(gca,'yscale','log')
%     ylim([1E-9 1E-2]);
%     Title('Pitch','position',[0.2 1E-3]);
    legend('Exp.','Bladed','CAsT')
    saveas(fig,'PSD-Pitch.bmp');

