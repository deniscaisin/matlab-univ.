clc;
clear;
close all;

%%Parameters
CG= -0.166;
rref= -0.166;  % distance from reference point(0,0,0) to CG
rcg= 0.166;  % distance from the CG to reference point (0,0,0)

%% Loading DATA
FastData=importdata(['C:\FAST\ReferenceToCGMatlab\Wave0_1T2_8.out']);
FastData=FastData.data;
TimeRef=FastData(:,1);
SurgeRef=FastData(:,2);
SwayRef=FastData(:,3);
HeaveRef=FastData(:,4);
RollRef=FastData(:,5);
PitchRef=FastData(:,6);
YawRef=FastData(:,7);
WaveRef=FastData(:,14);


%% Calculating Components of the  Rotational Matrix

alpha=RollRef;
beta=PitchRef;
gamma=YawRef;

r11= cosd(beta).*cosd(gamma);
r12= (sind(alpha).*sind(beta).*cosd(gamma))-(cosd(alpha).*sind(gamma));
r13= (cosd(alpha).*sind(beta).*cosd(gamma))+(sind(alpha).*sind(gamma));

r21= cosd(beta).*sind(gamma);
r22= (sind(alpha).*sind(beta).*sind(gamma))+(cosd(alpha).*cosd(gamma));
r23= (cosd(alpha).*sind(beta).*sind(gamma))-(sind(alpha).*cosd(gamma));

r31= -sind(beta);
r32= sind(alpha).*cosd(beta);
r33= cosd(alpha).*cosd(beta);
%% Calculating Translational Motion due to rotation


R= [r11 r12 r13;      %rotational Motion Matrix
    r21 r22 r23;
    r31 r32 r33];

Rot=R*[0;0;rref];    

n=length(Rot)/3;

Rotx=Rot(1:n);       %motion in surge
Roty=Rot((n+1):(2*n));    %motion in sway
Rotz=Rot((2*n+1):(3*n));    %motion in heave

Rotz=Rotz-CG;  % changing from the location of the CG to translational motion
%% Final translation motion due to translation and rotation:
TransVector= [SurgeRef+Rotx, SwayRef+Roty, HeaveRef+Rotz];

FinalVector= [TimeRef, TransVector,RollRef, PitchRef,YawRef];

Wave=[TimeRef,WaveRef];

save Wave0_1T2_8.dat -ascii FinalVector
save Wave0_1T2_8Wave.dat -ascii Wave








