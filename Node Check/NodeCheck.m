
clear
clc
load nodenew.txt;
load elementnew.txt;
% node=node_exp2013;

eid=elementnew;
node=nodenew;
plot3(node(:,2),node(:,3),node(:,4));
plot3(node(:,2),node(:,3),node(:,4),'o');
hold on;
for k=1:length(node(:,1));
text(node(k,2),node(k,3),node(k,4),num2str(k))
end
hold off
for i=1:length(eid(:,1))

line([node(eid(i,2),2),node(eid(i,3),2)],[node(eid(i,2),3),node(eid(i,3),3)],[node(eid(i,2),4),node(eid(i,3),4)]);


end