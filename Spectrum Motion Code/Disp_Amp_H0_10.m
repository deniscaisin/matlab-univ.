%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% This is used to calculate the magnitude of the motion
%%%% Required file: raw motion data H0.1T1.6.txt(n,7)
%%%%                raw wave data WH0.1T1.6(n,2)
%%%% Results: All_Disp_amp: Magnitude of the motion units(m,m,m,rad,rad,rad)
%%%%          All_Disp_pha: Phase diffference (wave)units (pi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc;
clear;
close all;
%%% Parameters
dt=0.01;   %%% Time step in sample;
Num_wave=7;   %%%% Number of the incident wave.
Num_period=7;  %%%% Number of the wave period used to analysis(will determin the length of the sample data->Becareful not longer than the total sample) 
Wave_period=[1.6 1.8 2.0 2.2 2.4 2.6 2.8]; %%%% Wave period.
C_num=7;  %%% Fixed number= number of columns in one motion file.
%% Read raw data
W(:,1:7)=load(['H0.1T1.6.out']);
W(:,8:14)=load(['H0.1T1.8.out']);
W(:,15:21)=load(['H0.1T2.0.out']);
W(:,22:28)=load(['H0.1T2.2.out']);
W(:,29:35)=load(['H0.1T2.4.out']);
W(:,36:42)=load(['H0.1T2.6.out']);
W(:,43:49)=load(['H0.1T2.8.out']);

wave(:,1:2)=load(['H0.1T1.6.wave']);
wave(:,3:4)=load(['H0.1T1.8.wave']);
wave(:,5:6)=load(['H0.1T2.0.wave']);
wave(:,7:8)=load(['H0.1T2.2.wave']);
wave(:,9:10)=load(['H0.1T2.4.wave']);
wave(:,11:12)=load(['H0.1T2.6.wave']);
wave(:,13:14)=load(['H0.1T2.8.wave']);
%%% Finish reading raw data;
%% Start to analysis
 for k=1:6 %%% used to loop from surge to yaw
ID_Disp=k;  %%% Surge=1,heave=3,pitch=5
Tenn=ID_Disp+1; %%% First colume is the time. 
%%% analysis each component of the motion in each wave period
for i=1:Num_wave
    j=C_num*(i-1)+Tenn;
    if Tenn==6
        act=W(:,j)-0.0093;  
    else                          
act=W(:,j);  %%% act is the displacement in each wave period;
    end

%%%%% FFT of the data-> determin the magnitude of the displacement
x=[];
X=[];
ampX=[];
[l,m]=size(act);
time=wave(:,1);    %%% Time 
a=Wave_period/dt; 
Len_sample=length(wave(:,1))-a(Num_wave)*Num_period; %%% Length of the data used to analysis.
t=time(Len_sample:Len_sample+a(i)*Num_period)';
x=act(Len_sample:Len_sample+a(i)*Num_period);  %%% substracted data used to analysis;

n=a(i)*Num_period+1;   
fs=1/dt;
df=fs/n;
f=df*(0:n-1);
X=fft(x);
ampX=abs(X)/(n/2);
[mm,nn]=max(ampX(2:n));
phaseX=angle(X);
Y=X';
n1(i)=nn+1; 

t=wave(Len_sample:Len_sample+a(i)*Num_period,1);
w=wave(Len_sample:Len_sample+a(i)*Num_period,i*2);


fs=1/dt;
df=fs/n;
f=df*(0:n-1);
Xw=fft(w);
ampXw=abs(Xw)/(n/2);
phaseXw=angle(Xw);
Y=Xw';

X2=zeros(size(X));
X2(1)=X(1);
X2(n1(i))=X(n1(i));X2(end-n1(i)+2)=X(end-n1(i)+2);
x2=ifft(X2);

%%%% Plot the time history of the motion
%%%% Plot parameters
Title_name=['T=1.6s';'T=1.8s';'T=2.0s';'T=2.2s';'T=2.4s';'T=2.6s';'T=2.8s'];
Disp_name=['Surge';'Sway ';'Heave';'Roll ';'Pitch';'Yaw  '];
Ylabel_name=['Disp.(m)  ';'Disp.(m)  ';'Disp.(m)  ';'Disp.(rad)';'Disp.(rad)';'Disp.(rad)'];
% fig=figure(i);
% set(gca,'FontSize',15);
% T_st=80/dt;T_end=100/dt;
% plot(time(T_st:T_end),act(T_st:T_end),'r','lineWidth',2);hold on
% plot(time(T_st:T_end),wave(T_st:T_end,2*i),'k');hold on
% 
% 
% xlabel('Time(s)','fontsize',15);
% ylim([-0.06 0.06]);
% xlim([80 100]);
% ylabel([Ylabel_name(k,:)],'fontsize',15);
%  legend([Disp_name(k,:)],'Wave');
% title(['Regular Wave H=0.1m ' Title_name(i,:)],'fontsize',15);
% 
% saveas(fig,[Disp_name(k,:),Title_name(i,:),'.bmp']);

average=ampX(1)/2;
amplitude=ampX(n1(i));
phai=-phaseX+phaseXw;
phase=phai(n1(i))/pi;

DisPhase(i,:)=phase;
DisAvrg(i,:)=average;
DisAmp(i,:)=amplitude;
hold off


end
 Location_max_f(k,:)=n1;
 All_Disp_amp(:,k)=DisAmp;
 All_Disp_pha(:,k)=DisPhase;
% switch Tenn
% case 2
% save Surge_Phase.txt -ascii DisPhase
% save Surge_Amp.txt -ascii DisAmp
% save Surge_Mean.txt -ascii DisAvrg
% 
% case 3
% save Sway_Phase.txt -ascii DisPhase
% save Sway_Amp.txt -ascii DisAmp
% save Sway_Mean.txt -ascii DisAvrg
% case 4
% save Heave_Phase.txt -ascii DisPhase
% save Heave_Amp.txt -ascii DisAmp
% save Heave_Mean.txt -ascii DisAvrg
% case 5
% save Roll_Phase.txt -ascii DisPhase
% save Roll_Amp.txt -ascii DisAmp
% save Roll_Mean.txt -ascii DisAvrg
% case 6
% save Pitch_Phase.txt -ascii DisPhase
% save Pitch_Amp.txt -ascii DisAmp
% save Pitch_Mean.txt -ascii DisAvrg
% 
% case 7
% save Yaw_Phase.txt -ascii DisPhase
% save Yaw_Amp.txt -ascii DisAmp
% save Yaw_Mean.txt -ascii DisAvrg
% end
 end
% NorTenAmp=TenAmp/rou/g/A/A/H;

save All_Disp_amp.txt -ascii All_Disp_amp
save All_Disp_pha.txt -ascii All_Disp_pha
